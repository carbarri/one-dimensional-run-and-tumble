//////////////////
///            ///
/// RaT-PEP-LJ ///
///            ///
//////////////////

// Authors: Christian Vanhille Campos, Carlos Miguel Barriuso Gutiérrez (carbarri@ucm.es).
// Description: this script implements on-lattice, one dimensional run-and-tumble particle dynamics with cluster mobility and atractive Lennard Jones potential for the particles.
// Version: 01 - working version

// Observaciones: 
	// Allow repetition of particles in order of movement.
	// A cluster is any group of 2 or more particles standing next to each other.
	// Con la fuerza, no el potencial, controlado por epsilon únicamente.
	// Añadido shift al potencial para que el minimo del potencial caiga en el primer vecino.
	// Probabilidad de salto P = abs(F)/Fmax con F = Fp + Flj. 
	// Para cutoff de potencial 3 el maximo de fuerza es ~0.13 -> Fmax ~ 1.13. 
	// El tercer vecino siente potencial si ponemos sigma = 3.5 en vez de = 3.
	// Corregido bug dirdist: habia que inicializar Ndir a {0,0,0}.
	// Ahora se guarda tambien la evolucion de la CSD y la CDD (Cluster Direction Distribution).
	// Ahora la cluster mobility se habilita en el archivo de input.

// Mejoras futuras:

	// Poner tiempo inicial y final de guardado de datos.
	// Habilitar seleccion de que archivos se guardan.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <signal.h>
#include <string.h>


//////////////////////////////
// DECLARACIÓN DE FUNCIONES //
//////////////////////////////

float pot(int sigma, float epsilon, float erre);											// Potencial continuo (no hace falta).
float potTruncShif(int sigma, float epsilon, float erre, float rcutoff);					// Potencial truncado y desplazado.
float LJfTruncShif(int sigma, float epsilon, float erre, float rcutoff);					// Potencial truncado y desplazado.
void generateTimeVector(int totSteps, int measures, char *name, char *outputdir);			// Saves to a file a vector with the measure times.
bool fileExists(const char * filename);														// Check if file exists.


int main(){


//////////////////////////////
// VARIABLES AND PARAMETERS //
//////////////////////////////


// System parameters

	char error[100]="¡ERROR!";
	char outputdir[200];						// Path to general output directory (e.g. ~/output_RaT/), specified in input file.
	char simulation_outputdir[200];				// Specific subdirectory for this simulation (with specific parameters)
	char name[100];								// Name of the run
	int DEBUG;									// Set = 1 for debug console output, = 2 for simulation status, = 0 for no output.

	float alpha;								// Tumble probability
	float fi;									// Particle density

	int CMOB;									// Cluster specific dynamics selector (1=enabled, 0=disabled)
	int IS;										// Initial state of the system: 0 -> random, 1 -> coarsened, 2 -> equidistant particles

	float sigma;								// Particle diameter in simulation length units
	float epsilon;								// Potential depth [KbT]
	float potcutoff;							// Potential cutoff (in sigmas)
	float Fp;									// Propulsion force

	int Tmax;									// Maximum number of time steps
	int T0 = 0;									// Initial time step. Changed if reading frame.
	int M;										// Number of particles
	int N;										// Number of slots, length of the lattice (in sigmas). Adapted from the number of particles and the density
	int measures;								// Number of measures, for physical quantities averages
	int outputStep;								// Number of terminal outputs for DEBUG = 2

	int rndSeed;								// Wether to randomize or not the seed. Do not randomize for reproducibility

	double total_time_spent = 0.0;				// Total time elapsed since beginning of the simulation

	scanf("Run name: %s\n", name);
	scanf("Output dir: %s\n", outputdir);
	scanf("Verbosity: %d\n", & DEBUG);

	scanf("Tumbling rate (alpha): %f\n",& alpha);
	scanf("Density (phi): %f\n", & fi);

	scanf("Potential depth (epsilon): %f\n", & epsilon);
	scanf("Potential radius (sigma): %f\n", & sigma);
	scanf("Potential cutoff (in sigmas): %f\n", & potcutoff);
	scanf("Propulsion force: %f\n", & Fp);

	scanf("Simulation time: %d\n", & Tmax);
	scanf("Number of particles (M): %d\n", & M);
	N = M/fi;
	scanf("Number of measures: %d\n", & measures);
	scanf("Number of outputs: %d\n", & outputStep);

	scanf("Random seed? (1=yes, 0=no): %d\n", & rndSeed);
	scanf("Cluster move? (1=yes, 0=no): %d\n", & CMOB);
	scanf("Initial state of the system (0=random, 1=coarsened, 2=equidistant, 3=read from file): %d\n", & IS);


	printf("Run name: %s\n", name);
	printf("Output dir: %s\n", outputdir);
	printf("Verbosity: %d\n", DEBUG);

	printf("Tumbling rate (alpha): %f\n", alpha);
	printf("Density (phi): %f\n",  fi);

	printf("Potential depth (epsilon): %f\n",  epsilon);
	printf("Potential radius (sigma): %f\n",  sigma);
	printf("Potential cutoff (in sigmas): %f\n", potcutoff);
	printf("Propulsion force: %f\n", Fp);

	printf("Simulation time: %d\n",  Tmax);
	printf("Lattice Size (N): %d\n",  N);
	printf("Number of particles (M): %d\n",  M);
	printf("Number of measures: %d\n",  measures);
	printf("Number of outputs: %d\n",  outputStep);

	printf("Random seed? (1=yes, 0=no): %d\n",  rndSeed);
	printf("Cluster move? (1=yes, 0=no): %d\n",  CMOB);
	printf("Initial state of the system (0=random, 1=coarsened, 2=equidistant, 3=read from file): %d\n",  IS);

	fflush(stdin);


// Simulation variables

	int i;										// Counter
	int Tint=Tmax/measures;						// Measuring interval (in time steps)
	int step;									// Time step variable
	int saveStep = measures;					// Number of saves of system state
	int Nsnaps = 1000;							// Number of snapshots at beginning and steady state (to limit size of output files)
	int Tsnaps = Tint/Nsnaps;					// Interval of time for taking snapshots of the system
	if (Tsnaps < 1) {Tsnaps = 1;}
	float rcutoff = potcutoff*sigma;			// Potential cutoff in simulation length units, not particle diameters
	// float maxProb = Fp;							// Maximum force (in magnitude) experienced by a particle, for normalization purposes when computing jumping probabilities.
	float maxProb = 1;							// Maximum force (in magnitude) experienced by a particle, for normalization purposes when computing jumping probabilities.
	for (float x = 1; x < rcutoff; x++) {maxProb += LJfTruncShif(sigma,epsilon,x,rcutoff);}
	if (DEBUG == 1) {printf("\n\nMaximum probability: %f\n\n",maxProb);}


// Data structures

	int array[N];								// Array of slots (lattice): 0 -> empty, n > 0 -> particle n
	int arraycluster[N];						// Vector with the clusters on the above array
	int C[N/2];									// Spatial correlations array
	int Nclusters=0;							// Number of clusters in the system
	int positions[M];							// Array of particles' positions
	int directions[M];							// Array of particles' swimming directions (1=+x; 0=-x;)
	int trueDirections[M];						// Array of particles' resulting moving directions (1=+x; 0=-x;)
	int order[M];								// Array with the ordinal position of each particle from left to right on the lattice
	int orderedParts[M];						// Array with the particles ordered according to their position on the lattice
	int cluster[M];								// Vector with the ID of the cluster the particles are in (0 for no cluster)
	int Nsize[M];								// Vector with the number of clusters of the same size (overall)
	int NsizeEvol[M];							// Vector with the number of clusters of the same size (at a specific time)
	int Ndir[3] = {0,0,0};						// Vector with the number of clusters in each direction (overall): +, -, 0
	float probabilities[M];						// Array with the jump probabilities for each particle
	float V[M];									// Array with the potential/force experienced by each particle

	// Initialize to 0
	memset(array, 0, sizeof(array));
	memset(arraycluster, 0, sizeof(arraycluster));
	memset(C, 0, sizeof(C));
	memset(positions, 0, sizeof(positions));
	memset(directions, 0, sizeof(directions));
	memset(trueDirections, 0, sizeof(trueDirections));
	memset(order, 0, sizeof(order));
	memset(orderedParts, 0, sizeof(orderedParts));
	memset(cluster, 0, sizeof(cluster));
	memset(Nsize, 0, sizeof(Nsize));
	memset(NsizeEvol, 0, sizeof(NsizeEvol));
	memset(probabilities, 0, sizeof(probabilities));
	memset(V, 0, sizeof(V));


// Output dir

	struct stat st = {0};

	if (stat(outputdir, &st) == -1) {
	    mkdir(outputdir, 0700);
	}

	sprintf(simulation_outputdir, "%s/sim_a%.3f_f%.3f_eps%.3f_t%e_M%d_Fp%.1f", outputdir, alpha, fi, epsilon, (float)Tmax, M, Fp);

	struct stat stbis = {0};

	if (stat(simulation_outputdir, &stbis) == -1) {
	    mkdir(simulation_outputdir, 0700);
	}

// Output files

	FILE *sc;
	char buffersc[200];												// Saves the CSD accumulated throughout the simulation
	sprintf(buffersc, "%s/sizedistr.dat", simulation_outputdir);

	FILE *scn;
	char bufferscn[200];												// Saves the CSD accumulated throughout the simulation
	sprintf(bufferscn, "%s/sizedistr_norm.dat", simulation_outputdir);

	FILE *dc;
	char bufferdc[200];												// Saves the direction distribution of clusters
	sprintf(bufferdc, "%s/dirdistr.dat", simulation_outputdir);

	FILE *scevol;
	char bufferscevol[200];											// Saves the temporal evolution of the CSD
	sprintf(bufferscevol, "%s/sizedistrevol.dat", simulation_outputdir);
	scevol=fopen(bufferscevol, "wb");

		fprintf(scevol, "step ");
		for (i=0; i<M; i++) {fprintf(scevol, "%d ", i+1);}
		fprintf(scevol, "\n");

	FILE *dcevol;
	char bufferdcevol[200];											// Saves the temporal evolution of the direction distirbution of clusters
	sprintf(bufferdcevol, "%s/dirdistrevol.dat", simulation_outputdir);
	dcevol=fopen(bufferdcevol, "wb");

		fprintf(dcevol, "step left still right\n");

	FILE *nc;
	char buffernc[200];												// Saves the temporal evolution of the total number of clusters in the system
	sprintf(buffernc, "%s/nclusters.dat", simulation_outputdir);
	nc=fopen(buffernc, "wb");

	FILE *snap;
	char buffersnap[200];											// Saves the state of the system for the initial steps - relaxation
	sprintf(buffersnap, "%s/system_beg.dat", simulation_outputdir);
	snap=fopen(buffersnap, "wb");

	FILE *snapbis;
	char buffersnapbis[200];											// Saves the state of the system for the final steps - steady state
	sprintf(buffersnapbis, "%s/system_ss.dat", simulation_outputdir);
	snapbis=fopen(buffersnapbis, "wb");

	FILE *corr;
	char buffercorr[200];											// Saves spatial correlations between particles, like g(r)
	sprintf(buffercorr, "%s/corr.dat", simulation_outputdir);
	corr=fopen(buffercorr, "wb");

	FILE *pars;
	char bufferpars[200];											// Saves the parameters of this simulation
	sprintf(bufferpars, "%s/parameters.dat", simulation_outputdir);


// Generamos vector de tiempos.

	//generateTimeVector(Tmax, measures, name, simulation_outputdir);

	printf("\nCreated directories\n\n");




////////////////
// SIMULATION //
////////////////

// Randomizamos la seed o no.
if (rndSeed==1){srand(time(NULL));} else {srand(0);}

// INITIAL STATE OF THE SYSTEM
/* DEBUG */	if (DEBUG==1){printf("\n\n|||||||||||INITIALIZATION|||||||||||||||||||||||||||||\n\n"); printf("Inital state: \n");}

	if (IS == 0) {								// RANDOM
		for (i=0; i<M; i++) {							// Particles are M, ordered from 1 to M
			int r1=rand()%N+1;							// Random position (1 to N, not 0 to N-1, !!)
			int r2=rand()%2;							// Random direction (1=+x; 0=-x;)
			int r3;

			if (r2==1) {r3=1;}
			else if (r2==0) {r3=-1;}

			if (array[r1-1]==0) {						// If that slot is free...
				positions[i]=r1;							// Save position
				directions[i]=r2;							// Save swimming direction
				trueDirections[i]=r3;						// Save real direction
				array[r1-1] = i+1;							// Fill slot
			}
			else {i=i-1;}								// Otherwise restart for same particle
		}
		int ipart = 0;
		for (i = 0; i < N; i++) {				// Compute order of particles
			if (array[i] != 0) {
				order[array[i]-1] = ipart+1;
				orderedParts[ipart] = array[i];
				ipart++;
			}
		}
	}
	else if (IS == 1) {							// COARSENED
		int sp = (int)((1-fi)/(float)2.0*N);
		for (i = 0; i < M; i++) {
			int r2=rand()%2;							// Random direction (1=+x; 0=-x;)
			int r3;
			if (r2==1) {r3=1;}
			else if (r2==0) {r3=-1;}
			positions[i] = sp+i;
			directions[i] = r2;
			trueDirections[i] = r3;
			array[sp+i-1] = i+1;
			order[i] = i+1;
			orderedParts[i] = i+1;
		}
	}
	else if (IS == 2) {							// EQUIDISTANT 			Note that for fi > 0.5 distance is 1...
		int stp = (int)floor(1/fi);
		for (i = 0; i < M; i++) {
			int r2=rand()%2;							// Random direction (1=+x; 0=-x;)
			int r3;
			if (r2==1) {r3=1;}
			else if (r2==0) {r3=-1;}
			positions[i] = i*stp+1;
			directions[i] = r2;
			trueDirections[i] = r3;
			array[i*stp] = i+1;
			order[i] = i+1;
			orderedParts[i] = i+1;
		}
	}
	else if (IS == 3) {							// READ FILE 			Note that for fi > 0.5 distance is 1...
		FILE *frame_file;
		char bufferframe0[192];
		sprintf(bufferframe0, "frame0_a%.3f_f%.3f_eps%.3f_t%d_M%d.dat", alpha, fi, epsilon, Tmax, M);
		frame_file = fopen(bufferframe0, "r");
		fscanf(frame_file, "%d ", &T0);
		for (i = 0; i < M; i++) {
			fscanf(frame_file, "%d %d ", &positions[i], &directions[i]);
			trueDirections[i] = directions[i];
			array[positions[i]-1] = i+1;
		}
 		fclose(frame_file);
		int ipart = 0;
		for (i = 0; i < N; i++) {				// Compute order of particles
			if (array[i] != 0) {
				order[array[i]-1] = ipart+1;
				orderedParts[ipart] = array[i];
				ipart++;
			}
		}
	}

	pars=fopen(bufferpars, "wb");
	fprintf(pars, "Debug variable: %d (0 = no info printed - 1 = some info printed - 2 = all info printed)\n", DEBUG);
	fprintf(pars, "Lattice size: %d\n", N);
	fprintf(pars, "Tumbling rate: %f\n", alpha);
	fprintf(pars, "Particle density: %f\n", fi);
	fprintf(pars, "Number of particles: %d\n", (int)M);
	fprintf(pars, "Total simulation time: %d\n", Tmax);
	fprintf(pars, "Number of measures: %d\n", measures);
	fprintf(pars, "Range of the LJ potential: %f\n", potcutoff);
	fprintf(pars, "LJ potential intensity parameter (epsilon): %f\n", epsilon);
	fprintf(pars, "LJ potential particle diameters parameter (sigma): %f\n", sigma);
	fprintf(pars, "Propulsion force: %f\n", Fp);
	fprintf(pars, "Beta - Inverse of the temperature energy: %f\n", 1.0);
	fprintf(pars, "Mu - Friction coefficient / mobility: %f\n", 1.0);
	fprintf(pars, "Translational diffusivity: %f\n", 0.0);
	fprintf(pars, "Mobility of the clusters: %d\n", CMOB);
	fprintf(pars, "Cluster cutoff distance: %f\n", 1.0);
	fprintf(pars, "Initial state of the system: %d (0 = random - 1 = gas - 2 = coarsened - 3 = equidistant - 4 = read from file)\n", IS);
	fprintf(pars, "Time step: %f\n", 1.0);
	fprintf(pars, "Snap measuring interval: %d\n", Tint);
	fprintf(pars, "Initial time: %d\n", T0);
	fclose(pars);

		// DEBUG

		if (DEBUG==1){
			printf("\n\n");

			int ii;
			for (ii=0; ii<N; ii++) {printf("%d ",array[ii]);}

			printf("\n");
			for (ii=0; ii<N; ii++) {
				if (array[ii]!=0) {
					if (directions[array[ii]-1]==1){printf("> ");}
					else if (directions[array[ii]-1]==0){printf("< ");}
					else {printf("* ");}			
				}
				else {printf("- ");}
			}

			printf("\n");
			for (ii=0; ii<N; ii++) {
				if (array[ii]!=0) {
					if (trueDirections[array[ii]-1]==1){printf("> ");}
					else if (trueDirections[array[ii]-1]==-1){printf("< ");}
					else {printf("| ");}
				}
				else {printf("- ");}
			}
			printf("\n\n");
		}





clock_t begin = clock();					/*For computing execution time.*/

/*DYNAMICS LOOP - TIME STEPS AND CHANGE OF POSITION AND DIRECTION: EVALUATE THE DYNAMICS*/
/* DEBUG */	if (DEBUG==1){printf("\n\n|||||||||||PARTICLE DYNAMICS |||||||||||||||||||||||||\n\n");}
/* DEBUG */	if (DEBUG==1){printf("\n\n>>>>>>>>>>>ENTERING DYNAMICS LOOP>>>>>>>>>>>>>>>>>>>>>\n\n");}

		// bool cond=true; // use with while for unlimited running.
for (step=0; step<Tmax; step++) {					/*Loop for time steps - OPEN*/
/* DEBUG */	if (DEBUG==1){printf("\n================STEP: %d ===========================\n",step);}

	///* DEBUG */	if (DEBUG==1){printf("\nStep: %d\n",step);}

	/*// DEBERIAMOS PODER QUITAR ESTO ACTUALIZANDO ON THE FLY NO?
	memset(array, 0, sizeof(array));
	memset(arraycluster, 0, sizeof(arraycluster));
	for (i=0; i<M; i++) {
		array[positions[i]-1]=i+1;
	}*/
	
	/*CHOOSE A RANDOM ORDER OF MOVEMENT*/
	/* DEBUG */	if (DEBUG==1){printf("\n------------Rnd order of movement---------\n");}

	int orderofmovement[M];					/*Choice of an order for moving the particles*/
	for (i=0; i<M; i++) {
		int par=rand()%M+1;					/*Random particle to move at turn i (1-M, not 0-m-1!!)*/
		orderofmovement[i]=par;				/*Save particle par in turn i (even if repetition happens)*/
	}


	// BAD JUMPS DEBUG
	if (DEBUG == 3){printf("\nSTEP: %d||||||||||||||||||||||||||||||||||||||||||||\n\n", step);}

	/* DEBUG */	if (DEBUG==1){printf("\n\n>>>>>>>>>>>ENTERING PARTICLE LOOP>>>>>>>>>>>>>>>>>>>>>\n\n");}

	/*PARTICLE LOOP: Movement of each particle, change each particle in the above order.*/

	int k;
	int c=1;
	for (k=0; k<M; k++) {					

		int ptm=orderofmovement[k]-1;			/*Particle to move*/
		/* DEBUG */	if (DEBUG==1){printf("\n~~~~~~~~~~~~~~~~PARTICLE: %d ~~~~~~~~~~~~~~~~~~~~~~~\n",ptm+1);}

		/* DEBUG */	if (DEBUG==1){printf("Position: %d\nOrientation: %d\n",positions[ptm],directions[ptm]);}

		/*TUMBLING: Allow for a change of direction*/
		/* DEBUG */	if (DEBUG==1){printf("\n------------Computing TUMBLING------------\n");}

		int prob=rand();

		/* DEBUG */	if (DEBUG==1){printf("Tumbling rand number: %f\n",prob/(float)RAND_MAX);}

		if (prob<=alpha*RAND_MAX) {
			int tmbl=rand();

			/* DEBUG */	if (DEBUG==1){printf("TUMBLE!\nOrientation rand number: %f\n",tmbl/(float)RAND_MAX);}

			if (tmbl<=0.5*RAND_MAX)
			{			// Esto se puede hacer con suma booleana.
				if (directions[ptm]==0)
				{
					directions[ptm]=1;
					/* DEBUG */	if (DEBUG==1){printf("NewOrientation: >\n");}
				}
				else
				{
					directions[ptm]=0;
					/* DEBUG */	if (DEBUG==1){printf("NewOrientation: <\n");}
				}
			}
			else
			{
				/* DEBUG */	if (DEBUG==1){printf("NewOrientation: -\n");}
			}
		}

		
		// Computing INTERACTION FORCES
		/* DEBUG */	if (DEBUG==1){printf("\n------------Computing EFFECT OF POTENTIAL-----------\n");}

		int v;
		int pos=positions[ptm]-1;
		V[ptm]=0;								/*Actually the force...*/

		for (v=1; v<rcutoff; v++) {				/*Check neighbours*/
		  int Vposplus=pos+v;
		  int Vposminus=pos-v;
		  if (Vposplus>=N) {Vposplus-=N;}		/*Periodic boundary*/
		  if (Vposminus<0) {Vposminus+=N;}		/*Periodic boundary*/
		  if (array[Vposplus]!=0) {V[ptm] = V[ptm] + LJfTruncShif(sigma,epsilon,v,rcutoff);}		/* Add right neighbours contribution */
		  if (array[Vposminus]!=0) {V[ptm] = V[ptm] - LJfTruncShif(sigma,epsilon,v,rcutoff);}		/* Add left neighbours contribution */
		}
			/* DEBUG */	if (DEBUG==1){printf("Fuerza sobre la particula: %f\n", V[ptm]);}


		// Computing DIRECTION
		/* DEBUG */	if (DEBUG==1){printf("\n------------Computing DIRECTION-----------\n");}

		int dirAux;
		if 		(directions[ptm] == 0){dirAux = -1;}	
		else if (directions[ptm] == 1){dirAux = 1;}
		else {printf("%s (dir = %d)\n", error, directions[ptm]);}

		probabilities[ptm] = Fp*dirAux + V[ptm];	
		probabilities[ptm] = probabilities[ptm]/maxProb;

			/* DEBUG */	if (DEBUG==1){printf("Probability: %f\n", probabilities[ptm]);}

		if 		(probabilities[ptm]<0) {trueDirections[ptm]=-1;}			// Moving left.	
		else if (probabilities[ptm]==0) {trueDirections[ptm]=0;}	// Not moving.
		else if (probabilities[ptm]>0) {trueDirections[ptm]=1;}		// Moving right
		else {printf("%s (prob = %f)\n", error, probabilities[ptm]);}

			/* DEBUG */	if (DEBUG==1){printf("Probability: %f\nMoving direction: %d\nParticle orientation: %d\n",probabilities[ptm],trueDirections[ptm],directions[ptm]);}


		/*Expected new position*/
		/* DEBUG */	if (DEBUG==1){printf("\n------------Computing new position--------\n");}

		int npos;															/*npos is the new expected position of the particle ptm*/
		if (positions[ptm]==1) {											/*Periodic boundary*/
			if (trueDirections[ptm]==-1) {npos=N;} 							
			else if (trueDirections[ptm]==1) {npos=positions[ptm]+1;}
			else if (trueDirections[ptm]==0) {npos=positions[ptm];}

		}
		else if (positions[ptm]==N) {										/*Periodic boundary*/
			if (trueDirections[ptm]==1) {npos=1;} 
			else if (trueDirections[ptm]==-1) {npos=positions[ptm]-1;}		
			else if (trueDirections[ptm]==0) {npos=positions[ptm];}
		}
		else {
			 if (trueDirections[ptm]==-1) {npos=positions[ptm]-1;} 			/*Rest of grid*/
			else if (trueDirections[ptm]==1) {npos=positions[ptm]+1;}
			else if (trueDirections[ptm]==0) {npos=positions[ptm];}
		}
		if (DEBUG==1){printf("New position: %d\n",npos);}


			// DEBUG BAD JUMPS
			if (DEBUG == 3){
				printf("\n~~Particle: %d~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n", ptm);

				printf("    Old Pos: %d \n    New Pos: %d\n    Jump Length: %d\n", positions[ptm], npos, abs(npos - positions[ptm]));					

				if (abs(npos - positions[ptm])>1)
				{
					printf("\nHORROR!\n\n");					
				}
			}


		/*EXCLUDED VOLUME: find if the movement is allowed*/
		/* DEBUG */	if (DEBUG==1){printf("\n------------Computing EXCLUDED VOLUME------\n");}

		int j, used=0;
		if (array[npos-1] != 0) {used=1;}					/*Check if that position is occupied*/
		if (used == 0) {									/*Position is free - check potential*/
			int probb = rand();

			if (DEBUG == 1){printf("Libre!\nJump rand number: %f\n",probb/(float)RAND_MAX);}
				

			if (probb<=fabsf(probabilities[ptm])*RAND_MAX) {	/*Movement is allowed*/
				if (DEBUG == 1){printf(" > JUMP! < \n");}
				positions[ptm]=npos; // JUMP!
				array[npos-1]=ptm+1;
				array[pos]=0;
			}
		}

		else {if (DEBUG==1){printf("Ocupado!\nJump rand number: -\n");}}

		if (DEBUG==1){printf("Position: %d\nOrientation: %d\n",positions[ptm],directions[ptm]);}

		// DEBUG: output to terminal.

		if (DEBUG == 1 || DEBUG == 3){
			// Particle ID
			printf("\n    Particle ID: ");
			for (int ii=0; ii<N; ii++) {printf("%d ",array[ii]);}
			printf("\n");

			// Orientation
			printf("\n    Orientation: ");
			for (int ii=0; ii<N; ii++) {
				if (array[ii]!=0) {
					if (directions[array[ii]-1]==1){printf("> ");}
					else if (directions[array[ii]-1]==0){printf("< ");}
					else {printf("* ");}			
				}
				else {printf("- ");}
			};
			printf("\n");

			// Direction of movement
			printf("\n    Mvmnt. dir:  ");
			for (int ii=0; ii<N; ii++) {
				if (array[ii]!=0) {
					if (trueDirections[array[ii]-1]==1){printf("> ");}
					else if (trueDirections[array[ii]-1]==-1){printf("< ");}
					else {printf("| ");}
				}
				else {printf("- ");}
			};

			// Order of movement
			printf("\n\n    Order of movement: ");
			for (int ii=0; ii<M; ii++) {printf("%d ",orderofmovement[ii]);}
			 printf("\n\n");
		}
	
		// if (k%2==0){system("@cls||clear");}

	}	/*PARTICLE LOOP: end.*/

	/* DEBUG */	if (DEBUG==1){printf("\n\n<<<<<<<<<<<EXITING PARTICLE LOOP<<<<<<<<<<<<<<<<<<<<<<\n\n");}


//////////////////////
// CLUSTER DYNAMICS //
//////////////////////

if (CMOB == 1 || CMOB == 0)
{
	/* DEBUG */	if (DEBUG==1){printf("\n\n|||||||||||CLUSTER DYNAMICS|||||||||||||||||||||||||||\n\n");}
	/* DEBUG */	if (DEBUG==1){printf("\n------------Evaluating clusters-----------\n\n");}

	// DEBERIAMOS PODER QUITAR ESTO ACTUALIZANDO ON THE FLY NO?
	// memset(array, 0, sizeof(array));
	// memset(arraycluster, 0, sizeof(arraycluster));
	/*for (i=0; i<M; i++) {
		array[positions[i]-1]=i+1;
	}*/


	// int d, p, pp, nn, j=0;
	// for (i=0; i<N; i++) {if (array[i]==0) {d=i; break;}}	/*Displacement to avoid separating a cluster due to end of count*/
	// for (i=0; i<N; i++) {
	// 	p=i+d;						/*Position*/
	// 	nn=p+1;						/*Next one*/
	// 	pp=p-1;						/*Previous one*/
	// 	if (p>=N) {p=p-N;}
	// 	if (nn>=N) {nn=nn-N;}
	// 	if (pp>=N) {pp=pp-N;}
	// 	if (pp<0) {pp=pp+N;}
	// 	if (DEBUG == 1) {printf("\n\nEvaluting position %d which after displacement is %d\n",i,p);}
	// 	if (array[p]!=0) {				/*Save the cluster the particle is in for each position*/
	// 		if (DEBUG == 1) {printf("Particle %d with neighbours %d and %d\n", array[p], array[pp], array[nn]);}
	// 		if (array[pp]!=0 || array[nn]!=0) {	/*Only if there is another particle before or after (cond. 4 cluster)*/
	// 			arraycluster[p]=j+1;
	// 			if (DEBUG == 1) {printf("It's part of cluster %d\n", j+1);}
	// 		}
	// 	}
	// 	else if (array[p]==0 && arraycluster[pp]!=0) {j++; if (DEBUG == 1) {printf("End of cluster %d, switching to %d\n", j, j+1);}}	/*If not change cluster*/
	// }
	// Nclusters=j;
	// if (arraycluster[p]!=0) {Nclusters++;}
	// if (DEBUG == 1) {printf("\n\nTotal number of clusters is %d\n", Nclusters);}


	/*ASSIGN CLUSTER NUMBER TO PARTICLES (0 IF FREE)*/
	// if (DEBUG == 1) {printf("\n\n~~~~~~~~~CLUSTER ARRAY:~~~~~~~~~\n");}
	// for (i=0; i<N; i++) {
	// 	for (j=0; j<M; j++) {
	// 		if (positions[j]-1==i) {cluster[j]=arraycluster[i];}
	// 	}
	// 	if (DEBUG == 1) {printf("%d ", arraycluster[i]);}
	// }



	// CHANGE POSITION OF CLUSTERS
	// /* DEBUG */	if (DEBUG==1){printf("\n\n------------Computing cluster mvmnt-------\n\n");}

	// int dir, size;
	// float clusterprob;
	// Ndir[0]=0;
	// Ndir[1]=0;
	// Ndir[2]=0;

	// for (int i = 0; i < M; ++i){NsizeEvol[i]=0;}
	// for (i=0; i<Nclusters; i++) 
	// {
	// 	dir=0;
	// 	size=0;
	// 	if (DEBUG==1){printf("\nEvaluating cluster %d: size %d and direction %d\n", i+1, size, dir);}

	// 	// Find the overall direction of movement for the cluster.
	//  	for (p=0; p<M; p++)
	//  	{				
	//  		if (cluster[p]==i+1) // Si la particula p esta en el cluster i+1.
	//  		{
	//  			size++;
	//  			if (directions[p]==0) {dir=dir-1;} 	// Si la particula se mueve a la izq.
	//  			else {dir=dir+1;}					// Si la particula se mueve a la drch.
	//  			if (DEBUG==1){printf("\tParticle %d is part of the cluster!\n\tNew size: %d\n\tNew direction: %d\n", p+1, size, dir);}
	//  		}
	//  	}
	// 	if (DEBUG==1){printf("Finished evaluating cluster %d: size %d and direction %d\n", i+1, size, dir);}


	// 	// Save number of clusters of same size at the time intervals.
	//  	if ((step+1)%Tint==0 && step>0) 
	//  		{
	//  			NsizeEvol[size-1]++;
	//  			Nsize[size-1]++;
	//  		}


	//  	// Save number of clusters in the same direction at the time intervals.
	//  	if ((step+1)%Tint==0 && step>0) {
	//  				if (dir<0) {Ndir[0]++;}
	//  				else if (dir==0) {Ndir[1]++;}
	//  				else if (dir>0) {Ndir[2]++;}
	//  	}	

	//  	/* DEBUG */ if ((step+1)%Tint==0 && DEBUG==1) {printf("\nCluster %d Size %d Direction %d -- N[%d]=%d Ndir[0]=%d Ndir[1]=%d Ndir[2]=%d\n",i+1,size,dir,size,Nsize[size-1],Ndir[0],Ndir[1],Ndir[2]);}


	//  	// Compute the probability for the cluster to jump.	
	//  	if (DEBUG==1){printf("\n\nEvaluating cluster jump\n");}
	//  	clusterprob=abs(dir)/(float)size;
	//  	if (DEBUG==1){printf("Jumping probability: %f\n",clusterprob);}
	//  	int rclust=rand();
	//  	if (DEBUG==1){printf("Random number: %f\n",rclust/(float)RAND_MAX);}
	//  	int cltop=0, clbot=N, condmovcl=1;
	// 	if (rclust<=clusterprob*RAND_MAX) {		Probability for whole cluster met
	//  	if (DEBUG==1){printf("Jumping is allowed by probability!\n\tInitial bounds: [%d-%d]\n",clbot,cltop);}
	//  	for (p=0; p<M; p++) {
	// 		if (cluster[p]==i+1) {
	// 			int posit=positions[p]-d;
	// 			if (posit<=0) {posit+=N;}
	// 	 		if (posit>cltop) {cltop=posit;}
	// 	 		if (posit<clbot) {clbot=posit;}
	// 	 		if (DEBUG==1){printf("\tNew bounds: [%d-%d]\n",clbot,cltop);}
	// 		}
	//  	}
	// 	cltop+=d; clbot+=d;
	// 	if (cltop>N) {cltop-=N;}
	//  	int ncltop=cltop+1;
	//  	if (ncltop>N) {ncltop=ncltop-N;}
	//  	int pclbot=clbot-1;
	//  	if (pclbot<=0) {pclbot=pclbot+N;}
	// 	if (DEBUG==1){printf("Bounds: [%d-%d]\n",clbot,cltop);}
	//  	for (p=0; p<M; p++) {
	//  		if (dir>0) {if (positions[p]==ncltop) {condmovcl=0; if (DEBUG==1){printf("Jump is not allowed!! Position %d occupied by particle %d\n",p,positions[p]);}}}
	//  		if (dir<0) {if (positions[p]==pclbot) {condmovcl=0; if (DEBUG==1){printf("Jump is not allowed!! Position %d occupied by particle %d\n",p,positions[p]);}}}
	//  	}
	//  	if (dir>0 && condmovcl!=0) {			/*Move whole cluster up one position*/
	//  		for (p=0; p<M; p++) {
	//  			if (cluster[p]==i+1) {
	//  				positions[p]=positions[p]+1;
	//  				if (positions[p]>N) {positions[p]=positions[p]-N;}
	//  			}
	//  		}
	//  	}
	//  	else if (dir<0 && condmovcl!=0) {		/*Move whole cluster down one position*/
	//  		for (p=0; p<M; p++) {
	//  			if (cluster[p]==i+1) {
	//  				positions[p]=positions[p]-1;
	//  				if (positions[p]<=0) {positions[p]=positions[p]+N;}
	//  			}
	//  		}
	//  	}
	//  	}
	// 	if (DEBUG==1) {printf("\nCluster: %d\nPosition: %d-%d\nDirection: %d\nSize: %d\nJumping probability: %f\nRandom: %f\n",i+1,clbot,cltop,dir,size,clusterprob,rclust/(float)RAND_MAX);}	
	//  }


	// for (i=0; i<N; i++) {array[i]=0; arraycluster[i]=0;}	/*Empty the array and the cluster sizes (reset)*/
	// if (DEBUG==1) {printf("\n\n~~~~~~~~~~~~RESULTING STATE~~~~~~~~~~~~\n\n");}
	// for (i=0; i<M; i++) {
	// 	array[positions[i]-1]=i+1;
	// }
	// if (DEBUG==1) {
	// 	for (i=0; i<N; i++) {printf("%d ",array[i]);}
	// }



	// Compute clusters (NEW)
	int j = 1;								// Cluster counter
	int size = 0, dir = 0;					// Cluster direction and size
	int rBound, lBound;						// Boundary particles IDs
	int visited[M];							// Array counting visits to avoid repetition of particles, initialize to 0
	int clusterParts[M];					// Array counting visits to avoid repetition of particles, initialize to 0
	memset(visited, 0, sizeof(visited));
	memset(clusterParts, 0, sizeof(clusterParts));
	memset(NsizeEvol, 0, sizeof(NsizeEvol));
	for (i = 0; i < M; i++) {				// Run through particles in order from left to right
		int pidx = orderedParts[i];					// ID of the current particle
		if (DEBUG == 1) {
			printf("Evaluating particle %d\n", pidx);
			if (visited[pidx-1] == 1) {printf("Visited already!\n");}
		}
		if (visited[pidx-1] == 1) {continue;}		// If already evaluated continue to next
		int nLeft, nRight;							// IDs of neighbours
		if (i == 0) {nLeft = orderedParts[M-1];}
		else {nLeft = orderedParts[i-1];}
		if (i == M-1) {nRight = orderedParts[0];}
		else {nRight = orderedParts[i+1];}
		int dLeft, dRight;
		dLeft = positions[pidx-1] - positions[nLeft-1];
		if (dLeft < 0) {dLeft = N + dLeft;}
		dRight = positions[nRight-1] - positions[pidx-1];
		if (dRight < 0) {dRight = N + dRight;}
		bool vLeft = false, vRight = false;
		if (visited[nLeft-1] == 1) {vLeft = true;}
		if (visited[nRight-1] == 1) {vRight = true;}
		bool inLeft = false, inRight = false;
		if (DEBUG == 1) {
			printf("Left neighbour is particle %d at distance %d\n", nLeft, dLeft);
			if (vLeft == true) {printf("It was visited already so it can't be part of this cluster (either too far and already checked or close now but because of a cluster move)\n");}
			printf("Right neighbour is particle %d at distance %d\n", nRight, dRight);
			if (vRight == true) {printf("It was visited already so it can't be part of this cluster (either too far and already checked or close now but because of a cluster move)\n");}
		}
		if ((dLeft == 1 && vLeft == false) || (dRight == 1 && vRight == false)) {			// It's part of a cluster
			if (DEBUG == 1) {printf("It's part of cluster %d\n", j);}
			if (dLeft == 1) {
				inLeft = true;							// Cluster extends to the left
				if (DEBUG == 1) {printf("Cluster extends to the left...\n");}
			}
			if (dRight == 1) {
				inRight = true;							// Cluster extends to the right
				if (DEBUG == 1) {printf("Cluster extends to the right...\n");}
			}
			cluster[pidx-1] = j;						// Assign cluster ID to particle
			visited[pidx-1] = 1;						// Save as visited
			size++;										// Increase size of cluster
			if (directions[pidx-1] == 0) {dir -= 1;}	// Modify direction of cluster
			else {dir++;}
			arraycluster[positions[pidx-1]-1] = j;		// Save slot as cluster ID
			clusterParts[size-1] = pidx;				// Save particle ID to cluster array
			rBound = pidx;								// Set current right bound particle
			lBound = pidx;								// Set current left bound particle
			if (DEBUG == 1) {printf("Cluster %d now has size %d, direction %d and limit particles [%d-%d]\n", j, size, dir, lBound, rBound);}
		}
		else {
			cluster[pidx-1] = 0;						// Assign cluster ID to particle
			visited[pidx-1] = 1;						// Save as visited
			arraycluster[positions[pidx-1]-1] = 0;		// Save slot as cluster ID
			if (DEBUG == 1) {printf("It's free\n");}
		}
		while (inLeft == true) {					// While still extending cluster
			if (DEBUG == 1) {printf("Still inside cluster %d, extending to the left\n", j);}
			if (visited[nLeft-1] == 1) {				// If already visited border particle stop extending
				inLeft = false;
				if (DEBUG == 1) {printf("Visited already particle %d\n", nLeft);}
			}
			else {										// Otherwise evaluate particle and decide whether to extend
				int npidx = nLeft;							// Change focus
				if (DEBUG == 1) {printf("Moving focus to particle %d, part of cluster %d\n", npidx, j);}
				cluster[npidx-1] = j;						// Assign cluster ID to particle
				visited[npidx-1] = 1;						// Save as visited
				size++;										// Increase size of cluster
				if (directions[npidx-1] == 0) {dir -= 1;}	// Modify direction of cluster
				else {dir++;}
				arraycluster[positions[npidx-1]-1] = j;		// Save slot as cluster ID
				clusterParts[size-1] = npidx;				// Save particle ID to cluster array
				lBound = npidx;								// Set current left bound particle
				if (DEBUG == 1) {printf("Cluster %d now has size %d, direction %d and limit particles [%d-%d]\n", j, size, dir, lBound, rBound);}
				int onp = order[npidx-1];
				if (onp == 1) {nLeft = orderedParts[M-1];}
				else {nLeft = orderedParts[onp-2];}
				dLeft = positions[npidx-1] - positions[nLeft-1];
				if (dLeft < 0) {dLeft = N + dLeft;}
				if (DEBUG == 1) {
					printf("Next neighbour to the left is %d, at distance %d\n", nLeft, dLeft);
					if (dLeft == 1) {printf("Cluster %d keeps extending to the left\n", j);}
					else {printf("Particle %d is the left end of cluster %d\n", npidx, j);}
				}
				if (dLeft == 1) {inLeft = true;}			// Cluster keeps extending
				else {inLeft = false;}						// This is the end of the cluster, stop extending
			}
		}
		while (inRight == true) {					// While still extending cluster
			if (DEBUG == 1) {printf("Still inside cluster %d, extending to the right\n", j);}
			if (visited[nRight-1] == 1) {				// If already visited border particle stop extending
				inRight = false;
				if (DEBUG == 1) {printf("Visited already particle %d\n", nLeft);}
			}
			else {										// Otherwise evaluate particle and decide whether to extend
				int npidx = nRight;							// Change focus
				if (DEBUG == 1) {printf("Moving focus to particle %d, part of cluster %d\n", npidx, j);}
				cluster[npidx-1] = j;						// Assign cluster ID to particle
				visited[npidx-1] = 1;						// Save as visited
				size++;										// Increase size of cluster
				if (directions[npidx-1] == 0) {dir -= 1;}	// Modify direction of cluster
				else {dir++;}
				arraycluster[positions[npidx-1]-1] = j;		// Save slot as cluster ID
				clusterParts[size-1] = npidx;				// Save particle ID to cluster array
				rBound = npidx;								// Set current right bound particle
				if (DEBUG == 1) {printf("Cluster %d now has size %d, direction %d and limit particles [%d-%d]\n", j, size, dir, lBound, rBound);}
				int onp = order[npidx-1];
				if (onp == M) {nRight = orderedParts[0];}
				else {nRight = orderedParts[onp];}
				dRight = positions[nRight-1] - positions[npidx-1];
				if (dRight < 0) {dRight = N + dRight;}
				if (DEBUG == 1) {
					printf("Next neighbour to the right is %d, at distance %d\n", nLeft, dLeft);
					if (dLeft == 1) {printf("Cluster %d keeps extending to the right\n", j);}
					else {printf("Particle %d is the right end of cluster %d\n", npidx, j);}
				}
				if (dRight == 1) {inRight = true;}			// Cluster keeps extending
				else {inRight = false;}						// This is the end of the cluster, stop extending
			}
		}
		if (size == 0) {
			if (DEBUG == 1) {printf("Not a cluster...\n");}
			cluster[pidx-1] = 0;						// Assign cluster ID to particle
			visited[pidx-1] = 1;						// Save as visited
			arraycluster[positions[pidx-1]-1] = 0;		// Save slot as cluster ID
			continue;
		}
		float prob = (float)CMOB*abs(dir)/(float)size;		// Jumping probability
		int rclust=rand();									// Random number
		if (DEBUG == 1) {
			printf("\nCluster %d finally has size %d, direction %d and limit particles [%d-%d]\n", j, size, dir, lBound, rBound);
			printf("Jumping probability is %f\n", prob);
			printf("Random number: %f\n", rclust/(float)RAND_MAX);
		}
		if (rclust<=prob*RAND_MAX) {					// Jump is allowed by probability
			if (DEBUG == 1) {printf("Jump is allowed by probability!\n");}
			if (order[lBound-1] == 1) {nLeft = orderedParts[M-1];}		// Compute neighbours to cluster
			else {nLeft = orderedParts[order[lBound-1]-2];}
			if (order[rBound-1] == M) {nRight = orderedParts[0];}
			else {nRight = orderedParts[order[rBound-1]];}
			dLeft = positions[lBound-1] - positions[nLeft-1];	// Compute distances to closest neighbours
			if (dLeft < 0) {dLeft = N + dLeft;}
			dRight = positions[nRight-1] - positions[rBound-1];
			if (dRight < 0) {dRight = N + dRight;}
			if (DEBUG == 1) {printf("Cluster %d has neighbours %d (left) at distance %d and %d (right) at distance %d\n", j, nLeft, dLeft, nRight, dRight);}
			if (dir < 0 && dLeft > 1) {							// If moving to the left and enough room
				if (DEBUG == 1) {printf("Jump to the left is allowed!\n");}
				for (int cid = 0; cid < size; cid++) {				// move all particles in cluster 1 step to the left
					if (DEBUG == 1) {printf("Particle # %d of cluster %d is particle %d with original position %d\n", cid+1, j, clusterParts[cid], positions[clusterParts[cid]-1]);}
					positions[clusterParts[cid]-1] -= 1;
					if (positions[clusterParts[cid]-1] <= 0) {positions[clusterParts[cid]-1] += N;}
					if (DEBUG == 1) {printf("New position %d\n", positions[clusterParts[cid]-1]);}
					arraycluster[positions[clusterParts[cid]-1]-1] = j;
					array[positions[clusterParts[cid]-1]-1] = clusterParts[cid];
				}
				if (positions[rBound-1] == N) {
					arraycluster[0] = 0;
					array[0] = 0;
				}
				else {
					arraycluster[positions[rBound-1]] = 0;
					array[positions[rBound-1]] = 0;
				}
				
			}
			else if (dir > 0 && dRight > 1) {					// If moving to the right and enough room
				if (DEBUG == 1) {printf("Jump to the right is allowed!\n");}
				for (int cid = 0; cid < size; cid++) {				// move all particles in cluster 1 step to the right
					if (DEBUG == 1) {printf("Particle # %d of cluster %d is particle %d with original position %d\n", cid+1, j, clusterParts[cid], positions[clusterParts[cid]-1]);}
					positions[clusterParts[cid]-1] += 1;
					if (positions[clusterParts[cid]-1] > N) {positions[clusterParts[cid]-1] -= N;}
					if (DEBUG == 1) {printf("New position %d\n", positions[clusterParts[cid]-1]);}
					arraycluster[positions[clusterParts[cid]-1]-1] = j;
					array[positions[clusterParts[cid]-1]-1] = clusterParts[cid];
				}
				if (positions[lBound-1] == 1) {
					arraycluster[N-1] = 0;
					array[N-1] = 0;
				}
				else {
					arraycluster[positions[lBound-1]-2] = 0;
					array[positions[lBound-1]-2] = 0;
				}
			}
		}
	 	if ((step+1)%Tint==0 && step>0) {					// Save measures
	 		NsizeEvol[size-1]++;
	 		Nsize[size-1]++;
	 		if (dir<0) {Ndir[0]++;}
	 		else if (dir==0) {Ndir[1]++;}
	 		else if (dir>0) {Ndir[2]++;}
	 	}
		j++;												// Change cluster
		size = 0;											// Reset size
		dir = 0;											// Reset direction
		if (DEBUG == 1) {printf("\nChanging cluster...\n\n");}
	}

	Nclusters = j-1;

	if (DEBUG==1) {
		printf("\n\n~~~~~~~~~~~~RESULTING STATE~~~~~~~~~~~~\n\n");
		for (i=0; i<N; i++) {printf("%d ",array[i]);}
		printf("\n");
		for (i=0; i<N; i++) {printf("%d ",arraycluster[i]);}
		printf("\n");
		printf("Number of clusters: %d\n", Nclusters);
	}
}


/////////////////////
// DATA & ANALYSIS //
/////////////////////

//if (step >= 999700)
//{
	/* code */

/* DEBUG */	if (DEBUG==1){printf("\n\n|||||||||||DATA & ANALYSIS||||||||||||||||||||||||||||\n\n");}

	/*SPATIAL CORRELATIONS*/
	int tag;
	int dist;
	int rest_i;
	int rest_j;
	if ((step+1)%Tint==0 && step>0) {
		for (dist=0; dist<N/2; dist++) {C[dist]=0;}
		for (tag=0; tag<N; tag++) {
			if (array[tag]==0) {continue;}
			for (dist=1; dist<N/2; dist++) {
				rest_i=tag+dist;
				rest_j=tag-dist;
				if (rest_i>=N) {rest_i=rest_i-N;}
				if (rest_j<=0) {rest_j=rest_j+N;}
				if (array[rest_i]!=0) {C[dist-1]++;}
				if (array[rest_j]!=0) {C[dist-1]++;}
			}
		}
		fprintf(corr, "%d	", step+1);
		for (dist=0; dist<N/2; dist++) {fprintf(corr, "%f	", C[dist]/(float)(2*M));}
		fprintf(corr, "\n");
	}

	if (DEBUG == 1) {printf("-----Correlations computed-----\n\n");}

	if (DEBUG == 1) {
		printf("STEP: %d\nMEASURING INTERVAL: %d\nrem((step+1)/Tint)== %d\nrem(step/Tint)== %d\nTsnaps = %d\n\n", step, Tint, (step+1)%Tint, step%Tint, Tsnaps);
	}




	// Save evolution of number, direction and size distribution (CSD) of clusters.
	// if ((step+1)%Tint==0 && step>0)
	if ((step+1)%Tint==0)
	{

		if (DEBUG == 1) {printf("-----Measuring stuff-----\n\n");}

		// Clusters Number Evolution-
		fprintf(nc, "%d	%d\n", step+1, Nclusters);

		if (DEBUG == 1) {printf("-----Number of clusters saved-----\n\n");}

		// CDD evol: cluster dir distribution.
		fprintf(dcevol, "%d %d %d %d\n", step+1, Ndir[0], Ndir[1], Ndir[2]);

		if (DEBUG == 1) {printf("-----Direction distribution evolution saved-----\n\n");}

		// CSD evol: cluster size distribution. 
		fprintf(scevol, "%d ", step+1);
		for (i=0; i<M; i++) {fprintf(scevol, "%d ", NsizeEvol[i]);}
		fprintf(scevol, "\n");

		if (DEBUG == 1) {printf("-----CSD evolution saved-----\n\n");}
	}	

	// Save snapshot of system
	if (step <= Tint && (step+1)%Tsnaps==0) 
	{

		if (DEBUG == 1) {printf("-----Taking snapshot initial-----\n\n");}

		int sn;
		fprintf(snap, "%d ", step);
		for (sn=0; sn<M; sn++) 
		{
			fprintf(snap, "%d %d ", positions[sn], directions[sn]);
		}
		fprintf(snap, "\n");

		if (DEBUG == 1) {printf("-----Snap initial taken-----\n\n");}
	}

	if (step >= Tmax-Tint && (step+1)%Tsnaps==0) 
	{

		if (DEBUG == 1) {printf("-----Taking snapshot steady state-----\n\n");}

		int sn;
		fprintf(snapbis, "%d ", step);
		for (sn=0; sn<M; sn++) 
		{
			fprintf(snapbis, "%d %d ", positions[sn], directions[sn]);
		}
		fprintf(snapbis, "\n");

		if (DEBUG == 1) {printf("-----Snap steady state taken-----\n\n");}
	}

	if (DEBUG == 1) {printf("-----DATA ANALYSIS IS DONE-----\n\n");}

//}




	///////////////////////
	// SIMULATION STATUS //
	///////////////////////

	if (DEBUG==2)
	{
		if (step%(Tmax/outputStep)==0) 
			{
				// printf("Sweep elapsed: %.4f\n",sweepCount/((float)Asize*AAsize*Fsize));
				printf("Simulation elapsed: %.4f\n",step/(float)Tmax);

				clock_t end = clock();										
				double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;	// Execution time of Tmax/outputStep steps.
				total_time_spent += time_spent;
				// printf("Begin: %ld\n",begin);
				// printf("End: %ld\n",end );
				// printf("Interval: %ld\n",end-begin );
				printf("Completed %d steps in %.4f seconds.\n", Tmax/outputStep, time_spent);
				printf("Exec. time of 1 step: %e seconds.\n",time_spent*outputStep/Tmax);
				printf("Total time spent: %e seconds.\n",total_time_spent);
				printf("Total estimated time: %e seconds.\n",time_spent*outputStep);
				printf("Estimated remaining time: %e seconds.\n\n",time_spent*outputStep-total_time_spent);
				// printf("Phi = %f\n",F[f_i]);
				// printf("Alpha = %f\n",A[a_i]);
				begin = clock();

			}
	}

}					/*End of the time loop - CLOSED*/
	/* DEBUG */	if (DEBUG==1){printf("\n\n<<<<<<<<<<<EXITING DYNAMICS LOOP<<<<<<<<<<<<<<<<<<<<<<\n\n");}
	

	// SAVE CLUSTER SIZE DISTRIBUTION (NSIZE)
	// sc=fopen(buffersc, "wb");
	// for (i=0; i<M; i++) {if (Nsize[i]!=0) {fprintf(sc, "%d	%d\n", i+1, Nsize[i]);}}
	// fclose(sc);

	/*SAVE CLUSTER SIZE DISTRIBUTION (NSIZE)*/
	sc=fopen(buffersc, "wb");
	scn=fopen(bufferscn, "wb");
	int norm=0;
	for (i=0; i<M; i++) {norm+=Nsize[i];}
	for (i=0; i<M; i++) 
	{
		if (Nsize[i]!=0) 
		{
			fprintf(sc, "%d	%.10f\n", i+1, Nsize[i]/(float)norm);
			fprintf(scn, "%f	%.10f\n", (i+1)/(float)M, Nsize[i]/(float)norm);
		}
	}
	fclose(sc);
	fclose(scn);


	// SAVE CLUSTER DIRECTION DISTRIBUTION
	
	dc=fopen(bufferdc, "wb");
	for (i=0; i<3; i++) 
	{
		if (Ndir[i]!=0) 
		{
			fprintf(dc, "%d	%d\n", i-1, Ndir[i]);
		}
	}

	fclose(dcevol);
	fclose(scevol);
	fclose(dc);
	fclose(nc);
	fclose(snap);
	fclose(corr);

	return 0;

}		/*Close main*/		





// DEFINICION DE FUNCIONES

// Potencial normal.
float pot(int sigma, float epsilon, float erre){
	return 	4*epsilon*(pow((sigma/erre),12)-pow((sigma/erre),6));
}

// Potencial shift 1. Shifteamos de tal manera que el minimo se alcance en r=1, para que el primer vecino sienta máxima repulsión. rmin = 2^(1/6) = 1.122462048.
// Para ello sumamos rmin y restamos 1.
float potShif(int sigma, float epsilon, float erre){
        return  pot(sigma, epsilon, erre + 0.122462048);
}

// Potencial truncado y shifteado sin desplazar: si usamos el que toma la media en el un intervalo obtenemos repulsion en primeros vecinos, cosa que no queremos.
float potTruncShif(int sigma, float epsilon, float erre, float rcutoff){
	if (erre<=rcutoff){
		float result = potShif(sigma,epsilon,erre)-potShif(sigma,epsilon,rcutoff);
		// printf("eps = %f\n pot = %f\n", epsilon, result);
		return result;
	}
	else{
		return 0;
	}
}

// Potencial truncado y shifteado sin desplazar: si usamos el que toma la media en el un intervalo obtenemos repulsion en primeros vecinos, cosa que no queremos.
float LJfTruncShif(int sigma, float epsilon, float erre, float rcutoff){
	if (erre<=rcutoff){
		float result = -24.0*epsilon*(2.0-pow(erre-1+pow(2.0,1.0/6.0),6.0))/pow(erre-1+pow(2.0,1.0/6.0),13.0);
		return result;
	}
	else{
		return 0;
	}
}

// Saves the times at which we measure onto a file.
void generateTimeVector(int totSteps, int measures, char *name, char *outputdir)
{
	char timevecfilename[200];
	sprintf(timevecfilename, "%s/%s_timeVect.dat", outputdir, name); 
	if (!fileExists(timevecfilename))
	{
			FILE *ti;
			char bufferti[200];
			sprintf(bufferti, "%s/%s_timeVect.dat", outputdir, name); 
			ti=fopen(bufferti, "wb");

			for (int i = 0; i < totSteps; ++i)
			{
				fprintf(ti, "%d\n", i*totSteps/measures);
			}

			fclose(ti);
	}
}

// Checks if a file exists.
bool fileExists(const char * filename)
{    
	FILE * file;
	if ( (file = fopen(filename, "r") ) == NULL)     
		{        
			return false;    
		}
		else
		{
			fclose(file); 
			return true;
		}
}


//// For clearing input buffer.
//void myflush ( FILE *in )
//{
//  int ch;
//
//  do
//    ch = fgetc ( in ); 
//  while ( ch != EOF && ch != '\n' ); 
//
//  clearerr ( in );
//}
//
//
//
//void mypause ( void ) 
//{ 
//  printf ( "Press [Enter] to continue . . ." );
//  fflush ( stdout );
//  getchar();
//} 
