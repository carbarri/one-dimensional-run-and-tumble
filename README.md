# One dimensional run and tumble

Simulations and cluster analysis of one dimensional on- and off-lattice systems of run and tumble particles with repulsive and attractive interactions.

Soft Matter, 2021, **17**, 10479 - 10491 ([arXiv](https://arxiv.org/abs/1912.01282)).

C.M. Barriuso-Gutiérrez, C. Vanhille, F. Alarcón, I. Pagonabarraga, R. Brito, C. Valeriani - _Collective motion of run-and-tumble repulsive and attractive particles in one-dimensional systems._

![system_animation](/doc/media/ezgif-2-a6e1ba36f406.gif)
![off-lattice_snapshots](/doc/media/figures/snapsOffLammps_a0.5.png)*Snapshots of the time evolution of the system for different tumbling rates ($`\alpha`$), densities ($`\phi`$) and interaction potentials (WCA -> repulsive, $`\epsilon\neq 0`$ -> attractive Lennard-Jones).*

![off-lattice_snapshots](/doc/media/figures/JyM_5.png)*Mean cluster size ($`M`$) and fraction of jammed particles ($`J`$) for the three systems studied as a function of the tumbling rate and interaction potentials.*
