########
# INIT #
########

# variable 	simName string	prod1_atrac

	variable alpha 	universe	0.001 0.001 0.001 0.001 0.01 0.01 0.01 0.01

	variable phi 	universe	0.1 0.1 0.4 0.4 0.1 0.1 0.4 0.4

	variable uLJcut universe	1.122462 2.5 1.122462 2.5 1.122462 2.5 1.122462 2.5

	variable epsilon equal 		1.0

	# variable alpha universe 0.001 0.001 0.001 0.001 0.001 0.001 0.002 0.002 0.002 0.002 0.002 0.002 0.005 0.005 0.005 0.005 0.005 0.005 0.01 0.01 0.01 0.01 0.01 0.01 0.02 0.02 0.02 0.02 0.02 0.02 0.05 0.05 0.05 0.05 0.05 0.05 0.1 0.1 0.1 0.1 0.1 0.1 0.2 0.2 0.2 0.2 0.2 0.2 0.5 0.5 0.5 0.5 0.5 0.5 1 1 1 1 1 1
	# variable phi universe 0.1 0.1 0.1 0.8 0.8 0.8 0.1 0.1 0.1 0.8 0.8 0.8 0.1 0.1 0.1 0.8 0.8 0.8 0.1 0.1 0.1 0.8 0.8 0.8 0.1 0.1 0.1 0.8 0.8 0.8 0.1 0.1 0.1 0.8 0.8 0.8 0.1 0.1 0.1 0.8 0.8 0.8 0.1 0.1 0.1 0.8 0.8 0.8 0.1 0.1 0.1 0.8 0.8 0.8 0.1 0.1 0.1 0.8 0.8 0.8
	# variable epsilon universe 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5 0.2 1 2.5


	#######################
	# SIMULATION PARAMETERS

	variable    timeStep        equal       0.02
	variable    RunTime         equal       5000
	variable    sample          equal       1
	variable    conf_sample     equal       1
	variable    rand_vel        equal       3297	

	variable 	L 				equal 		50.0
	variable    T     			equal       0.00001
	variable    Fa 				equal 		1.0
	variable 	LJcut			equal		${uLJcut}		# 1 = repul, 2.5 = attract
	variable	damp			equal		1.0
	variable 	clustCut		equal		1.3
	
	variable 	L2				equal		${L}/2
	variable	phi2			equal		${phi}^2
	variable 	sim_name		string		ts${timeStep}_rt${RunTime}_s${sample}_L${L}_T${T}_f${phi}_a${alpha}_e${epsilon}_Fa${Fa}_LJc${LJcut}_Cc${clustCut}

	####################
	# SIMULATION FOLDERS

	shell       mkdir	${sim_name}
	shell		cd 		${sim_name}
	shell		mkdir 	./Frames

	####################

	# log 		./log.lammps

	units       lj
	dimension   2  
	atom_style  atomic
	boundary    p p p


##############
# SYSTEM DEF #
##############

	lattice 		sq ${phi2}	# This is for creating a square lattice so we have to square de density so that it creates proper length of the simulation box/line.
	region 			mybox block -${L2} ${L2} -0.25 0.25	-0.25 0.25
	create_box 		2 mybox
	create_atoms 	1 box
	mass 			* 1.0

	set type 1 type/fraction 2 0.5 768976

	pair_style      lj/cut ${LJcut}
	pair_coeff      * * ${epsilon} 1.0
	pair_modify     shift yes
	neighbor 		1.5 bin

	velocity     all create ${T} ${rand_vel} mom yes rot yes dist gaussian


################
# SIM SETTINGS #
################

	timestep      ${timeStep}

	variable 	newType atom ((${alpha}>=random(0,1,725234))*(0.5>=random(0,1,313279))*((type==1)-(type==2))+type) 
	variable 	aForce	atom (-1)^(v_newType)

	# fix 		cachedForce all ave/atom 1 1 1 v_aForce
	# variable cFa atom 1.0*f_cachedForce

	compute		clusters 	all cluster/atom ${clustCut}
	compute 	MSD 		all msd
	compute 	RDF 		all rdf 100

	fix 		1 all langevin ${T} ${T} 1 7439
	fix         2 all nve
	fix			3 all addforce v_aForce 0.0 0.0 every 1
	fix			4 all enforce1d		# last fix always!

	fix 		5 all ave/time 100 1 100 c_MSD[1] file ./msd.dat mode scalar
	fix 		6 all ave/time 100 1 100 c_RDF[*] file ./rdf.dat mode vector

	#thermo_style  custom etotal ke pe ebond enthalpy temp press pxx pxy pxz pyy pyz pzz lx ly lz xy xz yz bonds xlo xhi ylo yhi zlo zhi ebond

	thermo_style  custom etotal ke pe enthalpy temp press pxx
	thermo_modify line multi flush yes
	thermo        ${sample}

	variable Etot 	equal etotal
	variable Ekin 	equal ke
	variable Epot	equal pe
	variable Enth 	equal enthalpy
	variable Temp 	equal temp
	variable Pres 	equal press
	variable Pxx 	equal pxx

	fix 7 all ave/time 10 100 1000 v_Etot v_Ekin v_Epot v_Enth v_Temp v_Pres v_Pxx file ./vars.dat

	dump          conf_run all custom ${conf_sample} ./Frames/Conf_Run.* id c_clusters type x vx fx
	dump_modify   conf_run sort id


#######
# RUN #
#######

	run           	${RunTime} pre no post no every 1 "set atom * type v_newType"

	write_data  	./Final_Configuration.data

	# shell			mv log_${sim_name}.lammps ./${sim_name}/log.lammps
	# shell			mv Final_Configuration_${sim_name}.data ./${sim_name}/Final_Configuration.data    

	clear

	shell cd ..

next alpha phi uLJcut
jump SELF