#!/bin/bash

# USAGE:
# bash runsim.sh <simulation_name> <number_of_mpithreads> <log_option>

simName=$(date '+%Y%m%d')"_"${1}


# Create simulation folder, copy input script and acces the simulation folder.

if [ ! -d ../../data/off-lattice_lammps/output/${simName} ]; 
then
	mkdir ../../data/off-lattice_lammps/output/${simName}
else
	echo "Folder exist. Overwrite? (y or n)"

	read answer

	if [[ "${answer}" == "n" ]];
	then
		echo "Aborting simulation."
		exit 1
	elif [[ "${answer}" == "y" ]]; 
	then
		echo "Starting simulation."		
		rm -r ../../data/off-lattice_lammps/output/${simName}
		mkdir ../../data/off-lattice_lammps/output/${simName}
	else
		echo "Bad input. Aborting simulation."
		exit 1
	fi
fi

cp 1DBD_enf.lammps ../../data/off-lattice_lammps/output/${simName}

cd ../../data/off-lattice_lammps/output/${simName}


# Generate SBATCH file with given options.

if [[ "${3}" == "nolog" ]];
then

	nohup time -f "\t%C, \t%E real,\t%U user,\t%S sys" -o sim.log mpirun -np ${2} ../../../../bin/off-lattice_lammps/lmp_mpi_capucuda_1dcnpc -log none -screen none -in 1DBD_enf.lammps &

elif [[ "${3}" == "log" ]];
then

	nohup time -f "\t%C, \t%E real,\t%U user,\t%S sys" -o sim.log mpirun -np ${2} ../../../../bin/off-lattice_lammps/lmp_mpi_capucuda_1dcnpc -in 1DBD_enf.lammps &

else
	echo "Bad input. Aborting simulation."
	exit 1
fi
