#!/bin/bash

# bash runusim.sh <simulation_name>

simName=$(date '+%Y%m%d')"_"${1}

if [ ! -d ../data/${simName} ]; 
then
	mkdir ../data/${simName}
else
	echo "Folder exist. Overwrite? (y or n)"

	read answer

	if [[ "${answer}" == "n" ]];
	then
		echo "Aborting simulation."
		exit 1
	elif [[ "${answer}" == "y" ]]; 
	then
		echo "Starting simulation."		
		rm -r ../data/${simName}
		mkdir ../data/${simName}
	else
		echo "Bad input. Aborting simulation."
		exit 1
	fi
fi

cp 1DBD_enf.lammps ../data/${simName}

cd ../data/${simName}

echo "#!/bin/bash

#SBATCH --job-name=${1}
#SBATCH --output=%A.out
#SBATCH --error=%A.err
#SBATCH --partition=short
#SBATCH --ntasks=1

# Add lines here to run your computations.
srun ../../bin/lmp_mpi_brigit_1dcnpc -in 1DBD_enf.lammps" > sendjob.sh

sbatch sendjob.sh

#START=$(date +%s.%N)

# mpirun -np ${2} ../../bin/lmp_mpi_brigit_1dcnpc -p ${2}x1 -log none -plog none -screen none -pscreen none -in 1DBD_enf_univ.lammps

#END=$(date +%s.%N)
#DIFF=$(echo "$END - $START" | bc)

# echo "Wall time: "${DIFF} >> simlog.txt
