#!/bin/bash

# USAGE:
# bash runusim.sh <simulation_name> <number_of_partitions> <mpi_threads_per_partition> <log_option>

simName=$(date '+%Y%m%d')"_"${1}

mpithreads=$((${2}*${3}))

# Create simulation folder, copy input script and acces the simulation folder.

if [ ! -d ../../data/off-lattice_lammps/output/${simName} ]; 
then
	mkdir ../../data/off-lattice_lammps/output/${simName}
else
	echo "Folder exist. Overwrite (o), continue (c) o abort (a)? If you continue and the simulation has the same parameters as a previous one (inside "${n_fol}"), LAMMPs will overwrite the folder."

	read answer

	if [[ "${answer}" == "a" ]];
	then
		echo "Aborting simulation."
		exit 1
	elif [[ "${answer}" == "o" ]]; 
	then
		echo "Starting simulation."		
		rm -r ../../data/off-lattice_lammps/output/${simName}
		mkdir ../../data/off-lattice_lammps/output/${simName}
	elif [[ "${answer}" == "c" ]]; 
		then		
			echo "Simulation will be placed in folder ../../data/off-lattice_lammps/output/${simName}."
			echo "Starting simulation."		
	else
		echo "Bad input. Aborting simulation."
		exit 1
	fi
fi

cp --backup=numbered 1DBD_enf_univ.lammps ../../data/off-lattice_lammps/output/${simName}

cd ../../data/off-lattice_lammps/output/${simName}


# Run simulation with given options.

if [[ "${4}" == "nolog" ]];
then

	nohup time -f "\t%C, \t%E real,\t%U user,\t%S sys" -o sim.log mpirun -np ${mpithreads} ../../../../bin/off-lattice_lammps/lmp_mpi_capucuda_1dcnpc -log none -plog none -screen none -pscreen none -p ${2}x${3} -in 1DBD_enf_univ.lammps &

elif [[ "${4}" == "log" ]];
then

	nohup time -f "\t%C, \t%E real,\t%U user,\t%S sys" -o sim.log mpirun -np ${mpithreads} ../../../../bin/off-lattice_lammps/lmp_mpi_capucuda_1dcnpc -p ${2}x${3} -in 1DBD_enf_univ.lammps &

else
	echo "Bad input. Aborting simulation."
	exit 1
fi
