#!/bin/bash

# bash runusim.sh <simulation_name> <number_of_partitions> <node_name> <log_option>

simName=$(date '+%Y%m%d')"_"${1}


# Create simulation folder, copy input script and acces the simulation folder.

if [ ! -d ../data/${simName} ]; 
then
	mkdir ../data/${simName}
else
	echo "Folder exist. Overwrite? (y or n)"

	read answer

	if [[ "${answer}" == "n" ]];
	then
		echo "Aborting simulation."
		exit 1
	elif [[ "${answer}" == "y" ]]; 
	then
		echo "Starting simulation."		
		rm -r ../data/${simName}
		mkdir ../data/${simName}
	else
		echo "Bad input. Aborting simulation."
		exit 1
	fi
fi

cp 1DBD_enf_univ.lammps ../data/${simName}

cd ../data/${simName}


# Generate SBATCH file with given options.

if [[ "${4}" == "nolog" ]];
then

echo "#!/bin/bash

#SBATCH --job-name=${1}
#SBATCH --output=%A.out
#SBATCH --error=%A.err
#SBATCH --partition=short
#SBATCH --ntasks=${2}

# Add lines here to run your computations.
mpirun -np ${2} ../../bin/lmp_mpi_brigit_1dcnpc -log none -plog none -screen none -pscreen none -p ${2}x1 -in 1DBD_enf_univ.lammps" > sendjob.sh

elif [[ "${4}" == "log" ]]; 
then

echo "#!/bin/bash

#SBATCH --job-name=${1}
#SBATCH --output=%A.out
#SBATCH --error=%A.err
#SBATCH --partition=short
#SBATCH --ntasks=${2}

# Add lines here to run your computations.
mpirun -np ${2} ../../bin/lmp_mpi_brigit_1dcnpc -p ${2}x1 -in 1DBD_enf_univ.lammps" > sendjob.sh


else
	echo "Bad input. Aborting simulation."
	exit 1
fi


# Send job to a specified node or in any one available.

if [[ "${3}" == "0" ]];
then
        sbatch sendjob.sh
else
        sbatch -w ${3} sendjob.sh
fi
