#!/bin/bash

# Simulation will be placed in .../1DRAT/data/on-lattice/output/<date>_<sweepName>

scriptName="ratpeplj_02_capuchino" 			# lammps executable name.
sweepName="prueba"							# name of the simulation.

numberOfThreads=4

alpha=(0.001)
phi=(0.8)
eps=(0 0.1 1 10)


#######################################


# outputDir="output"
execName=$scriptName"_"$sweepName
n_fol=$(date +%Y%m%d)_${sweepName}

if [ ! -d ../../data/on-lattice/output/${n_fol} ]; 
then
	mkdir "../../data/on-lattice/output/${n_fol}"
	mkdir "../../data/on-lattice/output/${n_fol}/jobs"
	echo ""
	echo "OK! Starting smulation :)"
	echo "Simulation will be placed in .../1DRAT/data/on-lattice/output/${n_fol}"
	echo ""
	echo "Folder exist. Overwrite? (y or n)."

	read answer

	if [[ "${answer}" == "n" ]];
	then
		echo "Aborting simulation :("
		exit 1
	elif [[ "${answer}" == "y" ]]; 
	then		
		rm -fr ../../data/on-lattice/output/${n_fol}
		mkdir "../../data/on-lattice/output/${n_fol}"
		mkdir "../../data/on-lattice/output/${n_fol}/jobs"
		echo ""
		echo "OK! Starting smulation :)"
		echo "Simulation will be placed in .../1DRAT/data/on-lattice/output/${n_fol}"
		echo ""
	else
		echo "Bad input. Aborting simulation :("
		exit 1
	fi
fi

cp ${scriptName} ../../data/on-lattice/output/${n_fol}/jobs
cp input.dat ../../data/on-lattice/output/${n_fol}/jobs

cd ../../data/on-lattice/output/${n_fol}/jobs



# mkdir ../jobs/$outputDir

#gcc -o ../jobs/$execName $scriptName".c" -lm


idx=($(seq -w 0 $(( ${#alpha[@]}*${#phi[@]}*${#eps[@]} - 1 )) ))
ii=0


# Generate jobs.
for i in $(seq -w 0 $(( ${#alpha[@]} - 1)) )
do
	for j in $(seq -w 0 $(( ${#phi[@]} - 1)) )
	do
		for k in $(seq -w 0 $(( ${#eps[@]} - 1)) )
		do
            jobName=$sweepName"_"${idx[$ii]}

			cp "input.dat" $jobName".input"

			echo "./"${scriptName}" < "$jobName".input" >> paralaunch.txt

			sed -i "1s/.*/Run name: "$sweepName"/" $jobName".input"

			# sed -i "2s/.*/Output dir: "$outputDir"/" $jobName".input"

			sed -i "4s/.*/Tumbling rate (alpha): "${alpha[${i#0}]}"/" $jobName".input"

			sed -i "5s/.*/Density (phi): "${phi[${j#0}]}"/" $jobName".input"

			sed -i "6s/.*/Potential depth (epsilon): "${eps[${k#0}]}"/" $jobName".input"

			((ii++))
		done
	done
done

# Launch jobs.

echo ${numberOfThreads} > procfile

nohup time -f "\t%C, \t%E real,\t%U user,\t%S sys" -o sim.log parallel --results output -j procfile < paralaunch.txt &
