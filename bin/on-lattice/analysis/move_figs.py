import numpy as np
import os
import sys
import glob

dirs = glob.glob('../../output_1DsppBD/%s'%(sys.argv[1]))
out = '../../figs'

for dir_name in dirs:
	os.system('cp %s/correlations.png %s/correlations/%s.png'%(dir_name, out, dir_name.split('output_1DsppBD/')[1]))
	os.system('cp %s/nclusters.png %s/nclusters/%s.png'%(dir_name, out, dir_name.split('output_1DsppBD/')[1]))
	os.system('cp %s/sizedistr.png %s/sizedistr/%s.png'%(dir_name, out, dir_name.split('output_1DsppBD/')[1]))
	os.system('cp %s/sizedistr_norm.png %s/sizedistr_norm/%s.png'%(dir_name, out, dir_name.split('output_1DsppBD/')[1]))
	os.system('cp %s/system_beg.png %s/system_beg/%s.png'%(dir_name, out, dir_name.split('output_1DsppBD/')[1]))
	os.system('cp %s/system_ss.png %s/system_ss/%s.png'%(dir_name, out, dir_name.split('output_1DsppBD/')[1]))