import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os
import sys

def csds():
	csdt = pd.read_csv('sizedistrevol.dat', index_col = 0, sep = ' ', header = 0)
	csdt = csdt.loc[:,'1':'500']
	cmap = matplotlib.cm.get_cmap('jet')
	x = []
	for ix in csdt.columns:
		x.append(int(ix))
	x = np.array(x)
	x = x/max(x)
	for i in csdt.index:
		P = csdt.loc[i,:]
		P = P/P.sum()
		plt.plot(x,P, '--', color = cmap(i/max(csdt.index)))
		plt.xlim(0,max(x))
		plt.ylim(0,1)
		plt.xlabel('Cluster size')
		plt.ylabel('P(l)')
		plt.title('CSD')
		plt.savefig('csds/%.6f.png'%(i/max(csdt.index)))
		plt.clf()

def main():
	wd = os.getcwd()
	path = sys.argv[1]
	os.chdir(path)
	os.system('mkdir csds')
	print('Plotting different times\n')
	csds()
	os.chdir('./csds')
	print('Compiling into gif\n')
	os.system('convert -delay 2 -loop 0 *.png csdt.gif')
	os.chdir(wd)

if __name__ == '__main__':
	main()