## This code computes J and M order parameters from sizedistr.dat and nclusters.dat (average).

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math
import os
import sys
import glob


phi = float(sys.argv[2])
alpha = float(sys.argv[1])
epsilons = [0, 0.1, 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
JM = pd.DataFrame(index = epsilons, columns = ['J', 'M'])
i=0
dirs = glob.glob('../../output_1DsppBD/sim_a%.3f_f%.3f*'%(alpha,phi))
outputlin = './'
outputlog = './'
outputJM = './'

for dir_name in dirs:
  alpha=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][2])
  phi=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][3])
  T=int(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][5])
  L=int(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][1])
  N=L*phi
  D=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][13])
  beta=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][11])
  v=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][10])
  eps=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][8])
  CMOB=int(float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][10]))
  IS=int(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][14])
  Tint=int(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][18])
  dt=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][17])
  file_sizes="%s/sizedistr_norm.dat"%(dir_name)
  file_nclust="%s/nclusters.dat"%(dir_name)
  if not (os.path.isfile(file_sizes)):
    print("File doesn't exist") 
    continue
  if not (os.path.isfile(file_nclust)):
    print("File doesn't exist") 
    continue
  if i==0:
    outputlin="../../figs/JM_sweepEpsilon_a%.3f_f%.3f_t%e_M%d_Fp%.1f.png"%(alpha,phi,T,N,v)
    outputlog="../../figs/JM_logx_sweepEpsilon_a%.3f_f%.3f_t%e_M%d_Fp%.1f.png"%(alpha,phi,T,N,v)
    outputJlog="../../figs/J_loglog_sweepEpsilon_a%.3f_f%.3f_t%e_M%d_Fp%.1f.png"%(alpha,phi,T,N,v)
    outputMlog="../../figs/M_loglog_sweepEpsilon_a%.3f_f%.3f_t%e_M%d_Fp%.1f.png"%(alpha,phi,T,N,v)
    outputJM="../../output_1DsppBD/JM_sweepEpsilon_a%.3f_f%.3f_t%e_M%d_Fp%.1f.dat"%(alpha,phi,T,N,v)
  print("Computing order parameters...")
  d_size=pd.read_csv(file_sizes,sep="\t",header=None,names=["l","p"])
  d_clust=pd.read_csv(file_nclust,sep="\t",header=None,index_col=0)
  Nc=d_clust.loc[Tint:,1]
  Nc_avg=sum(Nc)/len(Nc)
  p = d_size['p']
  l = d_size['l']
  p = p[l<=1]
  l = l[l<=1]
  pl = p*l
  M_this = sum(pl)
  J_this = M_this * Nc_avg
  print(eps,J_this,M_this)

  JM.loc[eps,'J'] = J_this
  JM.loc[eps,'M'] = M_this
  
  i=i+1

plt.figure(figsize = (10,8))
plt.plot(JM.index, JM['J'], 'bo-')
plt.plot(JM.index, JM['M'], 'ro-')
plt.title('Order parameters')
plt.xlabel(r'$\epsilon$')
plt.ylabel('J/M')
plt.xlim(0.09,11)
plt.ylim(0,1)
plt.savefig(outputlin)

plt.figure(figsize = (10,8))
plt.semilogx(JM.index, JM['J'], 'bo-')
plt.semilogx(JM.index, JM['M'], 'ro-')
plt.title('Order parameters')
plt.xlabel(r'$\epsilon$')
plt.ylabel('J/M')
plt.xlim(0.09,11)
plt.ylim(0,1)
plt.savefig(outputlog)

plt.figure(figsize = (10,8))
plt.loglog(JM.index, JM['J'], 'ro-')
plt.title(r'$\alpha = %.1f$ $\phi = %.1f$'%(alpha,phi))
plt.xlabel(r'$\epsilon$')
plt.ylabel('J')
plt.xlim(0.09,11)
plt.ylim(0.98,1)
plt.savefig(outputJlog)

plt.figure(figsize = (10,8))
plt.loglog(JM.index, JM['M'], 'ro-')
plt.title(r'$\alpha = %.1f$ $\phi = %.1f$'%(alpha,phi))
plt.xlabel(r'$\epsilon$')
plt.ylabel('M')
plt.xlim(0.09,11)
plt.ylim(0.05,1)
plt.savefig(outputMlog)

JM.to_csv(outputJM)
