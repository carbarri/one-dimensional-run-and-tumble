import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os
import sys
import glob

dirs = glob.glob('../../output_1DsppBD/%s'%(sys.argv[1]))

for dir_name in dirs:
	print('\nBuilding CSD temporal animation for %s\n'%(dir_name))
	os.system('python3 csd_animation.py %s'%(dir_name))
	print('Done!\n')