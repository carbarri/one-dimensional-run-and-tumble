#!/bin/bash

#SBATCH --job-name=noCMOBatrac
#SBATCH --output=../jobs/noCMOBatrac_%A_%a.out
#SBATCH --error=../jobs/noCMOBatrac_%A_%a.err
#SBATCH --array=0-134
#SBATCH --partition=short
#SBATCH --ntasks=1

# Print the task id.
b=$(seq -f '%03g' $SLURM_ARRAY_TASK_ID $SLURM_ARRAY_TASK_ID)
echo "My SLURM_ARRAY_TASK_ID: " $SLURM_ARRAY_TASK_ID
echo "My PADDED_SLURM_ARRAY_TASK_ID: " $b

# Add lines here to run your computations.
../jobs/ratpeplj_02_noCMOBatrac < "../jobs/noCMOBatrac_"$b".input"
