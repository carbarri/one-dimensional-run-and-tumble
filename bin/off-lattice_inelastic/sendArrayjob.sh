#!/bin/bash

#SBATCH --job-name=ISjym_2
#SBATCH --output=jobs/ISjym_2_%A_%a.out
#SBATCH --error=jobs/ISjym_2_%A_%a.err
#SBATCH --array=0-39
#SBATCH --partition=short
#SBATCH --ntasks=1

# Print the task id.
b=$(seq -f '%03g' $SLURM_ARRAY_TASK_ID $SLURM_ARRAY_TASK_ID)
echo "My SLURM_ARRAY_TASK_ID: " $SLURM_ARRAY_TASK_ID
echo "My PADDED_SLURM_ARRAY_TASK_ID: " $b

# Add lines here to run your computations.
cd jobs
./BDp_051_ISjym_2 < ISjym_2_${b}.input
