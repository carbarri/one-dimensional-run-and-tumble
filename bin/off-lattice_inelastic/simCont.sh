#!/bin/bash

mkdir ../../ISjym_2_cont
mkdir ../../ISjym_2_cont/scripts
cp BDp_051.c ../../ISjym_2_cont/scripts
cp simRun.sh simSet.sh input.dat ../../ISjym_2_cont/scripts
cp -r analysis ../../ISjym_2_cont/scripts

sed -i "1s/.*/Simulation name: "ISjym_2"_cont/" "../../"ISjym_2"_cont/scripts/input.dat"
sed -i "17s/.*/Initial state: 4/" "../../"ISjym_2"_cont/scripts/input.dat"
sed -i "4s/.*/sweepName="ISjym_2"_cont/" "../../"ISjym_2"_cont/scripts/simSet.sh"

cd ../../ISjym_2_cont/scripts
./simSet.sh
cd -

cd ../output_1DsppBD
dirnames=$(ls | grep sim)
for dir in $dirnames
do
	if [[ -d $dir ]];
	then
		cd $dir
		tail -n 1 system_ss.dat > ../../../ISjym_2_cont/scripts/jobs/frame0_${dir:4:14}${dir:30:40}.dat
		cd ..
	fi
done

