## This code computes J and M order parameters from sizedistr.dat and nclusters.dat (average).

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math
import os
import sys
import glob

dirs = glob.glob('../../output_1DsppBD/%s'%(sys.argv[1]))

OP=pd.DataFrame(data=np.zeros((18,5)));
phis = [0.1, 0.5, 0.8]
alphas = [0.001, 0.01, 0.1, 0.005, 0.05, 0.5, 0.008, 0.08, 0.8]
JM01 = pd.DataFrame(index = alphas, columns = ['J', 'M'])
JM05 = JM01.copy()
JM08 = JM01.copy()
i=0

for dir_name in dirs:
  alpha=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][2])
  phi=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][3])
  T=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][5])
  L=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][1])
  N=L*phi
  D=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][13])
  beta=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][11])
  v=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][10])
  eps=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][8])
  CMOB=int(float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][10]))
  IS=int(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][16].split('(')[0])
  Tint=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][18])
  dt=float(pd.read_csv("%s/parameters.dat"%(dir_name),engine='python',sep=": ",header=None,names=["Par","Value"])["Value"][17])
  file_sizes="%s/sizedistr_norm.dat"%(dir_name)
  file_nclust="%s/nclusters.dat"%(dir_name)
  if not (os.path.isfile(file_sizes)):
    print("File doesn't exist") 
    continue
  if not (os.path.isfile(file_nclust)):
    print("File doesn't exist") 
    continue
  if i==0:
    output="../../output_1DsppBD/OP_eps%.3f_t%e_M%d_Fp%.1f_IS%d.dat"%(eps,T,N,v,IS)
    outputJ="../../figs/J_eps%.3f_t%e_M%d_Fp%.1f.png"%(eps,T,N,v)
    outputM="../../figs/M_eps%.3f_t%e_M%d_Fp%.1f.png"%(eps,T,N,v)
    output01="../../output_1DsppBD/JM_f0.100_eps%.3f_t%e_M%d_Fp%.1f.dat"%(eps,T,N,v)
    output05="../../output_1DsppBD/JM_f0.500_eps%.3f_t%e_M%d_Fp%.1f.dat"%(eps,T,N,v)
    output08="../../output_1DsppBD/JM_f0.800_eps%.3f_t%e_M%d_Fp%.1f.dat"%(eps,T,N,v)
  print("Computing order parameters...")
  d_size=pd.read_csv(file_sizes,sep="\t",header=None,names=["l","p"])
  d_clust=pd.read_csv(file_nclust,sep="\t",header=None,index_col=0)
  Nc=d_clust.loc[Tint:,1]
  Nc_avg=sum(Nc)/len(Nc)
  p = d_size['p']
  l = d_size['l']
  p = p[l<=1]
  l = l[l<=1]
  pl = p*l
  M_this = sum(pl)
  J_this = M_this * Nc_avg
  #print(alpha,phi,Nc_avg,M_this,J_this)

  if phi == 0.1 or phi == 0.8:
    if alpha == 0.002 or alpha == 0.005 or alpha == 0.008 or alpha == 0.02 or alpha == 0.05 or alpha == 0.08 or alpha == 0.2 or alpha == 0.5 or alpha == 0.8:
      print(i,alpha,phi,M_this,J_this)
      OP.iloc[i,0]=alpha
      OP.iloc[i,1]=phi
      OP.iloc[i,2]=beta
      OP.iloc[i,3]=J_this
      OP.iloc[i,4]=M_this
      i = i+1

  # if phi == 0.1:
  #   JM01.loc[alpha,'J'] = J_this
  #   JM01.loc[alpha,'M'] = M_this
  # elif phi == 0.5:
  #   JM05.loc[alpha,'J'] = J_this
  #   JM05.loc[alpha,'M'] = M_this
  # elif phi == 0.8:
  #   JM08.loc[alpha,'J'] = J_this
  #   JM08.loc[alpha,'M'] = M_this

# plt.figure(figsize = (10,8))
# plt.semilogx(JM01.index, JM01['J'], 'bo-', label = '0.1')
# plt.semilogx(JM05.index, JM05['J'], 'ro-', label = '0.5')
# plt.semilogx(JM08.index, JM08['J'], 'go-', label = '0.8')
# plt.legend()
# plt.title('J')
# plt.xlabel(r'$\alpha$')
# plt.ylabel('J')
# plt.xlim(0.0005,1)
# plt.ylim(0,1)
# plt.savefig(outputJ)

# plt.figure(figsize = (10,8))
# plt.semilogx(JM01.index, JM01['M'], 'bo-', label = '0.1')
# plt.semilogx(JM05.index, JM05['M'], 'ro-', label = '0.5')
# plt.semilogx(JM08.index, JM08['M'], 'go-', label = '0.8')
# plt.legend()
# plt.title('M')
# plt.xlabel(r'$\alpha$')
# plt.ylabel('M')
# plt.xlim(0.0005,1)
# plt.ylim(0,1)
# plt.savefig(outputM)

OP.to_csv(output)
# JM01.to_csv(output01)
# JM05.to_csv(output05)
# JM08.to_csv(output08)
